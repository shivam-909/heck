package heck

import "errors"

func Wrap(message string, err error) error {
	return errors.New(message + "::" + err.Error())
}

func IsAny(err error, targets ...error) bool {
	for _, t := range targets {
		if err == t {
			return true
		}
	}
	return false
}
